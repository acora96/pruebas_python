#MANEJAR DATOS DE FINSA

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sklearn

#desactivar el warning de la funcion recortar del pandas
#https://www.dataquest.io/blog/settingwithcopywarning/
pd.set_option('mode.chained_assignment', None)

#CARGAR DATOS
datos=pd.read_csv("/Users/Adrian/Documents/Desarrollo/pruebas_python/data/formadora.csv",thousands=',')
    #print(datos)
print(datos.head())
print(datos.info())
    #print(type(datos))
    # #print(datos['$Date']) #seleccionar una columna
Tiempo=datos['$Time']
Fecha=datos['$Date']
PV_Peso_formadora_1=datos['S08033WC1-PV']
PV_Humedad_formadora_1=datos['S08033MC3-PV']
PV_Altura_entrada_formadora_1=datos['S08033GC4-PV']
PV_Altura_salida_formadora_1=datos['S08033GC5-PV']
PV_Rasurado_formadora_1=datos['S08033GC5A-PV']
PV_Velocidad_cinta_formadora_1=datos['S08033M04SI-PV']
PV_Nivel_silo_formadora_1=datos['S08033LI1-PV']
PV_Velocidad_linea_formacion=datos['S08050M01SI-PV']

#Tiempo_total=Tiempo.str.slice(0,2).multiply(60**2)
Tiempo_total=pd.to_datetime(Fecha).dt.dayofyear.multiply(24*(60**2))
Tiempo_total=Tiempo_total.add(pd.to_numeric(Tiempo.str.slice(0,2)).multiply(60**2)) #horas
Tiempo_total=Tiempo_total.add(pd.to_numeric(Tiempo.str.slice(3,5)).multiply(60)) #minutos
Tiempo_total=Tiempo_total.add(pd.to_numeric(Tiempo.str.slice(6,8))) #segundos

#Comprobación
a,b=0,0
for i_item,item in Tiempo_total.iteritems():
    #print(Tiempo_total[i_item])
    b=item
    if b<a:
        print('MAL')
    a=item

#plt.plot(Tiempo_total)
#plt.show()

#plt.plot(Tiempo_total,PV_Peso_formadora_1)
#plt.show()

i=1
def recorta(Serie,max_dif=2):
    for i in range(1,Serie.shape[0]):
        a=Serie[i-1]
        b=Serie[i]
        if abs(a-b)>max_dif:
            Serie[i]=Serie[i-1]
    return Serie

PV_Peso_formadora_1=recorta(Serie=PV_Peso_formadora_1,max_dif=2)
plt.plot(Tiempo_total,PV_Peso_formadora_1)

PV_Humedad_formadora_1=recorta(Serie=PV_Humedad_formadora_1,max_dif=2)
plt.plot(Tiempo_total,PV_Humedad_formadora_1)

plt.show()


for i in range(1,PV_Peso_formadora_1.shape[0]):
    a=PV_Peso_formadora_1[i-1]
    b=PV_Peso_formadora_1[i]
    if abs(a-b)>2:
        #print(PV_Peso_formadora_1[i])
        PV_Peso_formadora_1[i]=PV_Peso_formadora_1[i-1]


#BUCLES
#for i_row,row in datos.iterrows():
#    print(datos['$Time'][i_row])

#for i_item,item in Tiempo_total.iteritems():
#    print(Tiempo_total[i_item])

plt.plot(Tiempo_total,PV_Peso_formadora_1)
plt.show()



y_plot=sklearn.preprocessing.minmax_scale(pd.to_numeric(PV_Peso_formadora_1))
plt.plot(Tiempo_total,y_plot)

y_plot=sklearn.preprocessing.minmax_scale(pd.to_numeric(PV_Humedad_formadora_1))
plt.plot(Tiempo_total,y_plot)

y_plot=sklearn.preprocessing.minmax_scale(pd.to_numeric(PV_Altura_entrada_formadora_1))
plt.plot(Tiempo_total,y_plot)

plt.show()





datos.shape[0]

Tiempo_total.shape[0]