#Lo mismo que el anterior pero con una función senoidal


#entorno /Users/Adrian/Documents/Desarrollo/pruebas_python/entorno_2/bin/python3.6
#Getting started: 30 seconds to Keras
#https://keras.io/

#Linear Regression using Keras and Python
#https://heartbeat.fritz.ai/linear-regression-using-keras-and-python-7cee2819a60c

import numpy as np
import matplotlib.pyplot as plt

x_train = np.arange(0,4*np.pi,(2*np.pi/250))

def f1(x,a,y_0):
    np.random.RandomState(seed=2)
    y_val=np.arange(x.shape[0], dtype='f')
    y_train=np.arange(x.shape[0], dtype='f')

    for i in range(0,x.shape[0]):
        y_val[i]=np.sin(x[i])*np.array(a)+y_0
        y_train[i]=(y_val[i]+(np.random.rand(1,1)-0.5)).squeeze() #ruido
    y_val=np.squeeze(y_val)  #reducción de dimensiones ver método .shape
    y_train=np.squeeze(y_train)
    return y_val, y_train

y_val, y_train=f1(x=x_train,a=1.5,y_0=10)

"""plt.plot(x_train,y_val,'--')
plt.plot(x_train,y_train,'.')
plt.show()"""

#PASAR LOS DATOS A PANDAS
import pandas as pd
df= pd.DataFrame({'x': x_train,'y': y_val, 'y_noise': y_train})
df.head()

x_train=df.x
y_train=df.y_noise
y_val=df.y

#KERAS
from keras.models import Sequential
from keras.layers import Dense #para añadir capas linea a linea
import keras
import keras.backend as kb
import tensorflow as tf

#crear modelo
model = Sequential()

#definir las capas de la red neuronal
#función de activación relu --> F(x) = max(0,x)
#es necesario definir el tamaño de la entrada y la salida
#los tamaños y conexiones intermedias lo pilla automático por la densidad de capas
model.add(Dense(32, activation=tf.nn.relu, input_shape=[1]))
model.add(Dense(32, activation=tf.nn.relu))
model.add(Dense(32, activation=tf.nn.relu))
model.add(Dense(1))

#Optimizador del modelo: RMSprop (Root Mean Square Propagation)
#optimizer = tf.keras.optimizers.RMSprop(0.0099)
optimizer = tf.keras.optimizers.RMSprop(learning_rate=0.009, rho=0.6)

#Compilar el modelo
model.compile(loss='mean_squared_error',optimizer=optimizer)

#Ajustar el modelo
model.fit(x_train,y_train,epochs=500)

#batch, solo una vez
#model.train_on_batch(x_train,y_train)

#predicción
y_pred=model.predict(x_train)
y_pred=pd.Series(np.squeeze(y_pred))

plt.plot(x_train,y_val)
plt.plot(x_train,y_pred)
plt.plot(x_train,y_train,'.')
plt.legend(['y_val','y_pred','y_train'])
plt.show()